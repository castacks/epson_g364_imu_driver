//==============================================================================
//
// 	main_csvlogger.c - Epson IMU sensor test application
//                   - This program initializes the Epson IMU and 
//                     sends sensor output to CSV file
//
//
//  THE SOFTWARE IS RELEASED INTO THE PUBLIC DOMAIN.
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
//  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, NONINFRINGEMENT, 
//  SECURITY, SATISFACTORY QUALITY, AND FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT
//  SHALL EPSON BE LIABLE FOR ANY LOSS, DAMAGE OR CLAIM, ARISING FROM OR IN CONNECTION
//  WITH THE SOFTWARE OR THE USE OF THE SOFTWARE.
//
//==============================================================================
#include <stdint.h>
#include <stdio.h>
#include <time.h>

#include "hcl.h"
#include "hcl_gpio.h"
#include "sensor_epsonCommon.h"

#include <termios.h> // Low-level functions for UART communication
#include "hcl_uart.h"
int comPort;
// Modify below as needed for hardware 
const char *IMUSERIAL = "/dev/ttyUSB0";
const int IMUBAUD = B460800;

// Determines the number of samples to readout before exiting the program
const unsigned int NUM_SAMPLES = 1000;
    
int main(int argc, char *argv[])
{
    int i;
    unsigned int sample = 0;
    signed short readData[SENSOR_READ_LEN];
    double ppSensorData[6];
    unsigned short chksum16_ver;

    // 1) Initialize the Seiko Epson HCL layer
    printf("\r\nInitializing HCL layer...");
    if (!seInit()){
        printf("\r\nError: could not initialize the Seiko Epson HCL layer. Exiting...\r\n");
        return -1;
    }
    printf("...done.\r\n");

    // 2) Initialize the GPIO interfaces, For GPIO control of pins SPI CS, RESET, DRDY
    printf("\r\nInitializing GPIO interface...");
    if (!gpioInit()){
        printf("\r\nError: could not initialize the GPIO layer. Exiting...\r\n");
        return -1;
    }
    printf("...done.");

    // 3) Initialize UART Interface
    printf("\r\nInitializing UART interface...");
    comPort = uartInit(IMUSERIAL, IMUBAUD);
    if(comPort == -1)
    {   
        printf("\r\nError: could not initialize UART interface. Exiting...\r\n");
        return -1;
    }
    printf("...done.");

    // 4) Power on sequence - force sensor to config mode, HW reset sensor
    //      Check for errors
    printf("\r\nChecking sensor NOT_READY status...");
    if(!sensorPowerOn())
    {
        printf("\r\nError: failed to power on Sensor. Exiting...\r\n");
        uartRelease(comPort);
        return -1;
    }
    printf("...done.");
    
    // Initialize sensor with desired settings
    printf("\r\nInitializing Sensor...");
    if(!sensorInit())
    {
        printf("\r\nError: could not initialize Epson Sensor. Exiting...\r\n");
        uartRelease(comPort);
        return -1;
    }
    else
    {
        printf("...Epson IMU initialized.");        
    }
    // Initialize text files for data logs
    const time_t date   = time(NULL);                   // Functions for obtaining and printing time and date
    struct tm tm        = *localtime(&date);
    char  EpsonlogName[128];
    char  EpsonRawName[128];
    
    // Create Epson IMU Data Log
    sprintf(EpsonlogName, "EpsonLog-%4d-%02d-%02d_T%d:%d:%d.csv", tm.tm_year+1900, tm.tm_mon+1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
    FILE *EpsonLog     = fopen(EpsonlogName, "w");
    fprintf(EpsonLog,"Date: ");
    fprintf(EpsonLog, "%s", ctime(&date));
    fprintf(EpsonLog,"\r\nEpson IMU\r\nSAMPLE,\tXGYRO,\tYGYRO,\tZGYRO,\tXACCL,\tYACCL,\tZACCL,\tCount");

    //sprintf(EpsonRawName, "EpsonRaw-%d-%d-%dT%d%d%d.csv", tm.tm_year+1900, tm.tm_mon+1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
    //FILE *EpsonRaw     = fopen(EpsonRawName, "w");
    //fprintf(EpsonRaw,"Date: ");
    //fprintf(EpsonRaw, "%s", ctime(&date));

#if defined G350 || defined V340
    //fprintf(EpsonRaw,"Epson IMU\r\nSAMPLE,\tND_FLAG,\tTEMPC,\tX_GYRO,\tY_GYRO,\tZ_GYRO,\tX_ACCL,\tY_ACCL,\tZ_ACCL,\tGPIO,\tCOUNT");
#else //G354/G364/G352/G362/G320
    //fprintf(EpsonRaw,"\r\nEpson M-IMU\r\nSAMPLE,\tX_GYRO1,\tX_GYRO0,\tY_GYRO1,\tY_GYRO0,\tZ_GYRO1,\tZ_GYRO0,\tX_ACCL1,\tX_ACCL0,\tY_ACCL1,\tY_ACCL0,\tZ_ACCL1,\tZ_ACCL0,\tCOUNT,\tIMU_CHKSM16,\tHOST_CHKSM16");
#endif

    printf("\r\n...Epson IMU Logging.\r\n");
    sensorStart();
   
    while (1)
    {
        // For SPI interface, check if DRDY pin asserted
        // For UART interface, check if UART recv buffer contains a sensor sample packet
        if(sensorDataReady())
        {
#if defined G350 || defined V340 // 16-bit Sensor Output
            sensorDataReadBurstN(readData, SENSOR_READ_LEN);

            ppSensorDataRead16N(ppSensorData, readData, 2); // GyroX is located at index 2 of readData
            //fprintf(EpsonRaw, "\r\n%u,\t0x%04hX,\t0x%04hX,\t0x%04hX,\t0x%04hX,\t0x%04hX,\t0x%04hX,\t0x%04hX,\t0x%04hX,\t0x%04hX,\t0x%04hX", sample, readData[0], readData[1], readData[2], readData[3], readData[4], readData[5], readData[6], readData[7], readData[8],  (unsigned short)readData[9]);
            fprintf(EpsonLog, "\r\n%u,\t%+08f,\t%+08f,\t%+08f,\t%+08f,\t%+08f,\t%+08f,\t%5d", sample, ppSensorData[0], ppSensorData[1], ppSensorData[2], ppSensorData[3], ppSensorData[4], ppSensorData[5], (unsigned short)readData[9]);
#endif

#if defined G354 || defined G364 || defined G352 || defined G362 || defined G320 // 32-bit Sensor Output
            sensorDataReadBurstN(readData, SENSOR_READ_LEN);
            unsigned short tmp16 = calChecksum16(readData, SENSOR_READ_LEN-1);
            ppSensorDataRead32N(ppSensorData, readData, 0);

            fprintf(EpsonLog, "\r\n%u,\t%+08f,\t%+08f,\t%+08f,\t%+08f,\t%+08f,\t%+08f,\t%5d,\t0x%04hX,\t0x%04hX", sample, ppSensorData[0], ppSensorData[1], ppSensorData[2], ppSensorData[3], ppSensorData[4], ppSensorData[5], (unsigned short)readData[12],
            (unsigned short)readData[13], tmp16);
            if (tmp16 == (unsigned short)readData[SENSOR_READ_LEN-1])
                fprintf(EpsonLog,", OK");
            else
                fprintf(EpsonLog,", ERROR");
#endif

            sample++;
        }
        if (sample > (NUM_SAMPLES-1))
            break;
    }
    
    const time_t end = time(NULL);                    // Functions for obtaining and printing time and data
    tm  = *localtime(&end);
    fprintf(EpsonLog, "\r\nEnd: ");
    fprintf(EpsonLog, "%s", ctime(&end));
    //fprintf(EpsonRaw, "\r\nEnd: ");
    //fprintf(EpsonRaw, "%s", ctime(&end));
    
    sensorStop();
    sleep(1);
    uartRelease(comPort);
    gpioRelease();
    seRelease();
    fclose(EpsonLog);
    //fclose(EpsonRaw);
    printf("\r\nThe following arguments were passed to main(): ");
    for(i=1; i<argc; i++) printf("%s ", argv[i]);
    printf("\r\n");
    return 0;
}
