//==============================================================================
//
// 	sensor_epsonG350.h - Epson G350 sensor specific definitions
//
//
//  THE SOFTWARE IS RELEASED INTO THE PUBLIC DOMAIN.
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
//  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, NONINFRINGEMENT, 
//  SECURITY, SATISFACTORY QUALITY, AND FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT
//  SHALL EPSON BE LIABLE FOR ANY LOSS, DAMAGE OR CLAIM, ARISING FROM OR IN CONNECTION
//  WITH THE SOFTWARE OR THE USE OF THE SOFTWARE.
//
//==============================================================================
#ifndef EPSONG350_H_
#define EPSONG350_H_
#include<math.h>
#include<stdio.h>

#define TRUE 1
#define FALSE 0

#define IMU_MODEL              G350
#define EPSON_ACCL_SF         (.125)
#define EPSON_GYRO_SF         (.0125)

// G350 sensor read output is 16-bit only
// Define length in 16-bit words
// And the output packet contents may contain data in addition to gyro & accel
// # Bytes = ND_FLAG(2) + Temp(2) + Gyro(6) + Accel(6) + GPIO(2) + Count(2) 
// Do not count Header/Delimiter byte which is discarded by low-level function
#define SENSOR_READ_LEN 10
// This is defined in bytes and includes Header/Delimiter byte
#define BURSTLEN 22



/*                                      -- Commands --
    - ADDR_ address byte of transfer to select the register to access
    - VAL_  data byte of transfer to write to the register selected 
     
    - All accesses are 16 bit transfers
    - For SPI IF:
        - For SPI write accesses - 8-bit address with msb=1b (can be even or odd) + 8-bit write data
                                 - No response
        - For SPI read accesses - 8-bit address with msb=0b(even only) + 8-bit dummy data
                                - Response is transferred on MOSI on next SPI access
                                - Return value is 16-bit read data (high byte + low byte)    
    - For UART IF:
        - For UART write accesses - 8-bit address with msb=1b(can be even or odd) + 8-bit write data + Delimiter Byte
                                  - No response
        - For UART read accesses - 8-bit address with msb=0b(even only) + 8-bit dummy data + Delimiter Byte
                                 - Response is transferred immediately
                                 - Return value consists of Register Read Address + 16-bit read data (high byte + low byte) + Delimiter Byte
    
    - NOTE: G354/G364/G352/G362/G320 have Register Address Maps that depend on the WINDOW_ID (page) */


// WINDOW_ID 0
#define ADDR_FLAG                  0x00     // FLAG(ND/EA) (W0)
#define ADDR_TEMP_LOW              0x02     // TEMPC Byte0 (W0)
#define ADDR_TEMP_HIGH             0x02     // TEMPC Byte1 (W0)
#define ADDR_XGYRO_HIGH            0x04     // XGYRO Byte0 (W0)
#define ADDR_XGYRO_LOW             0x04     // XGYRO Byte1 (W0)
#define ADDR_YGYRO_HIGH            0x06     // YGYRO Byte0 (W0)
#define ADDR_YGYRO_LOW             0x06     // YGYRO Byte1 (W0)
#define ADDR_ZGYRO_HIGH            0x08     // ZGYRO Byte0 (W0)
#define ADDR_ZGYRO_LOW             0x08     // ZGYRO Byte1 (W0)
#define ADDR_XACCL_HIGH            0x0A     // XACCL Byte0 (W0)
#define ADDR_XACCL_LOW             0x0A     // XACCL Byte1 (W0)
#define ADDR_YACCL_HIGH            0x0C     // YACCL Byte0 (W0)
#define ADDR_YACCL_LOW             0x0C     // YACCL Byte1 (W0)
#define ADDR_ZACCL_HIGH            0x0E     // ZACCL Byte0 (W0)
#define ADDR_ZACCL_LOW             0x0E     // ZACCL Byte1 (W0)
#define ADDR_GPIO                  0x10     // GPIO  (W0)
#define ADDR_COUNT                 0x12     // COUNT (W0)
#define ADDR_SIG_CTRL_LO           0x32     // SIG_CTRL Byte0 (W0)
#define ADDR_SIG_CTRL_HI           0x33     // SIG_CTRL Byte1 (W0)
#define ADDR_MSC_CTRL_LO           0x34     // MSC_CTRL Byte0 (W0)
#define ADDR_MSC_CTRL_HI           0x35     // MSC_CTRL Byte1 (W0)
#define ADDR_SMPL_CTRL_LO          0x36     // SMPL_CTRL Byte0 (W0)
#define ADDR_SMPL_CTRL_HI          0x37     // SMPL_CTRL Byte1 (W0)
#define ADDR_FILTER_CTRL_LO        0x38     // FILTER_CTRL Byte0 (W0)
#define ADDR_MODE_CTRL_LO          0x38     // MODE_CTRL Byte0 (W0)
#define ADDR_MODE_CTRL_HI          0x39     // MODE_CTRL Byte1 (W0)
#define ADDR_UART_CTRL_LO          0x3A     // UART_CTRL Byte0 (W0)
#define ADDR_UART_CTRL_HI          0x3B     // UART_CTRL Byte1 (W0)
#define ADDR_DIAG_STAT             0x3C     // DIAG_STAT Byte0 (W0)
#define ADDR_GLOB_CMD_LO           0x3E     // GLOB_CMD Byte0 (W0)
#define ADDR_GLOB_CMD_HI           0x3F     // GLOB_CMD Byte1 (W0)
#define ADDR_COUNT_CTRL_LO         0x50     // COUNT_CTRL Byte0 (W0)

#define CMD_BURST                  0x20     // BURST

#define CMD_EN_NDFLAGS             0x7E     // Write value for SIG_CTRL_HI to Enables new data (ND) flags in FLAG for Gyros, Accelerometers
#define CMD_EN_BRSTDATA_LO         0x03     // Write value for BURST_CTRL1_LO to enable CHKSM, and COUNT bytes in burst mode
#define CMD_EN_BRSTDATA_HI         0x30     // Write value for BURST_CTRL1_HI to enable GYRO, and ACCL registers in burst mode (0xB0 for FLAG as well)
#define CMD_WINDOW0                0x00     // Write value for WIN_CTRL to change to Window 0
#define CMD_WINDOW1                0x01     // Write value for WIN_CTRL to change to Window 1
#define CMD_DRDY_GPIO1             0x04     // Write value for MSC_CTRL_LO to enable active low DRDY on GPIO1
#define CMD_32BIT                  0x30     // Write value for BURST_CTRL2_HI to enable 32 bit mode for gyro and accl data
#define CMD_BEGIN_SAMPLING         0x01     // Write value for MODE_CMD_HI to begin sampling
#define CMD_END_SAMPLING           0x02     // Write value for MODE_CMD_HI to stop sampling
#define CMD_SOFTRESET              0x80     // Write value for GLOB_CMD_LO to issue Software Reset
#define CMD_FLASHTEST              0x08     // Write value for MSC_CTRL_HI to issue Flashtest
#define CMD_SELFTEST               0x04     // Write value for MSC_CTRL_HI to issue Selftest

// Write values for ADDR_SMPL_CTRL_HI to set Output Rate
#define CMD_RATE1000               0x01     // TAP>=2
#define CMD_RATE500                0x02     // TAP>=4
#define CMD_RATE250                0x03     // TAP>=8
#define CMD_RATE125                0x04     // TAP>=16
#define CMD_RATE62_5               0x05     // TAP>=32
#define CMD_RATE31_25              0x06     // TAP>=64
#define CMD_RATE15_625             0x07     // TAP=128

// Write values for FILTER_CTRL_LO to set Filter
#define CMD_FLTAP2                 0x01
#define CMD_FLTAP4                 0x02
#define CMD_FLTAP8                 0x03
#define CMD_FLTAP16                0x04
#define CMD_FLTAP32                0x05
#define CMD_FLTAP64                0x06
#define CMD_FLTAP128               0x07

// MODE STAT
#define VAL_SAMPLING_MODE          0x00
#define VAL_CONFIG_MODE            0x04


#endif /* EPSONG350_H_ */
