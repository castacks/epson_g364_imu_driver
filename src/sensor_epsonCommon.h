//==============================================================================
//
// 	sensor_epsonCommon.h - Epson IMU sensor specific definitions common 
//                      for all IMU models
//
//
//  THE SOFTWARE IS RELEASED INTO THE PUBLIC DOMAIN.
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
//  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, NONINFRINGEMENT, 
//  SECURITY, SATISFACTORY QUALITY, AND FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT
//  SHALL EPSON BE LIABLE FOR ANY LOSS, DAMAGE OR CLAIM, ARISING FROM OR IN CONNECTION
//  WITH THE SOFTWARE OR THE USE OF THE SOFTWARE.
//
//==============================================================================
#ifndef EPSONCOMMON_H_
#define EPSONCOMMON_H_

#include<math.h>
#include<stdio.h>

#define TRUE 1
#define FALSE 0

#ifdef G350
#include "sensor_epsonG350.h"
#elif G352
#include "sensor_epsonG352.h"
#elif G362
#include "sensor_epsonG362.h"
#elif G354
#include "sensor_epsonG354.h"
#elif G364DC0
#include "sensor_epsonG364DC0.h"
#elif G364DCA
#include "sensor_epsonG364DCA.h"
#elif G320
#include "sensor_epsonG320.h"
#else /* V340 */
#include "sensor_epsonV340.h"
#endif

#define DELAY_EPSON_RESET           10                                     // Milliseconds Reset Pulse Width
#define EPSON_POWER_ON_DELAY        800                                    // Milliseconds
#define EPSON_FLASH_TEST_DELAY      5                                      // Milliseconds
#define EPSON_SELF_TEST_DELAY       80                                     // Milliseconds
#define EPSON_FILTER_DELAY          1                                      // Milliseconds

#define EpsonStall()                seDelayMicroSecs(EPSON_STALL)          // Required delay between bus cycles for serial timings

struct EpsonOptions{
  // SIG_CTRL
  int enable_xgyro, enable_ygyro, enable_zgyro;
  int enable_xaccel, enable_yaccel, enable_zaccel;
  int enable_temp;
  int enable_xgyro_delta, enable_ygyro_delta, enable_zgyro_delta;
  int enable_xaccel_delta, enable_yaccel_delta, enable_zaccel_delta;
  
  // MSC_CTRL
  int ext_sel;
  int ext_pol;
  int drdy_on;
  int drdy_pol;
  int flash_test, self_test;
  
  // SMPL_CTRL
  int dout_rate;

  // FILTER_CTRL
  int filter_sel;
  
  // UART_CTRL
  int baud_rate;
  int auto_start;
  int uart_auto;
  
  // BURST_CTRL1
  int flag_out, temp_out, gyro_out, accel_out, gyro_delta_out, accel_delta_out;
  int gpio_out, count_out, checksum_out;
  
  // BURST_CTRL2
  int temp_bit, gyro_bit, accel_bit, gyro_delta_bit, accel_delta_bit;
  
  // POL_CTRL
  int invert_xgyro, invert_ygyro, invert_zgyro, invert_xaccel, invert_yaccel, invert_zaccel;

  //DLT_CTRL
  int dlt_ovf_en;
  int dlt_range_ctrl;
};

struct EpsonData {
  float temperature;
  float gyro_x, gyro_y, gyro_z;
  float accel_x, accel_y, accel_z;
  float gyro_delta_x, gyro_delta_y, gyro_delta_z;
  float accel_delta_x, accel_delta_y, accel_delta_z;
  int count;
};

int sensorDataReadBurstNOptions(struct EpsonOptions, struct EpsonData*);
int sensorInitOptions(struct EpsonOptions);
int sensorHWReset(void);
int sensorPowerOn(void);
int sensorInit(void);
void registerDump(void);
void registerWriteByte(unsigned char, unsigned char, unsigned char, unsigned int);
unsigned short registerRead16(unsigned char, unsigned char, unsigned int);
void sensorStart(void);
void sensorStop(void);
void sensorReset(void);
int sensorFlashTest(void);
int sensorSelfTest(void);
int sensorDataReady(void);
void sensorDataReadBurstN(signed short [], unsigned int); // Not supported for G350/V340 SPI IF. Use sensorDataReadN() instead.
unsigned short calChecksum16(unsigned short [], unsigned int);
//void sensorDataReadN(signed short [], unsigned int, unsigned char); // For G350/V340 SPI IF only, moved to sensor_epsonSpi
void ppSensorDataRead32N(double [], signed short [], unsigned char); // 32-bit sensor output not supported for G350 or V340
void ppSensorDataRead16N(double [], signed short [], unsigned char);

#endif /* EPSONCOMMON_H_ */
